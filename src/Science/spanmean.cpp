#include "../Science.hpp"
#include "math.h"
#include "../Par_util.hpp"
#include "stdio.h"
#include "../Split_reader.hpp"
#include "../T_util.hpp"
#include "../Parformer.hpp"
#include "../Sorter.hpp"
#include <numeric>


using blitz::Array;
using blitz::firstDim;
using blitz::secondDim;
using blitz::thirdDim;

using TArrayn::DTArray;


    /* This file contains functions to compute spanwise (y) and streamwise (x) 
    averages, and to write them appropriately.
    The overall approach is consistent with other similar functions within SPINS. 
    Each function takes a target DTArray and the field to be averaged. The function 
    mutates the target array such that every x-z (y-z) slice contains the spanwise (streamwise)
    average. These functions are called compute_average_y() and compute_average_x() respectively.

    Finally, there are two functions which compute (but do not write) the variances along a particular dimension
    that are included for convenience.
    */ 



void compute_average_y(TArrayn::DTArray & avg, const TArrayn::DTArray & field){


    //Permute array and sum over the i and j indices.
    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk; 

    //Blitz++ requires you to sum over the last entry in the array
    //Since we are summing over the second index, their suggested workaround 
    //is change out the indices so the reduced tensor has a firstindex and a secondindex
    //and not a first index and a third index

    //This would need to be modified in the event of a non-uniform grid in the spanwise.
    //When has that ever happened though.....

    //Array<double,2> xzslice(Nx_,Nz_);


    int GNx = 0;
    int Nx_ = field.extent(firstDim);
    int Nz_ = field.extent(thirdDim);

    MPI_Allreduce(&Nx_,&GNx,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

    DTArray * tmp2D;

    tmp2D = alloc_array(GNx,1,Nz_);
    (*tmp2D)(blitz::Range::all(),0,blitz::Range::all()) = mean(field(ii,kk,jj),kk);

    //xzslice = mean(field(ii,kk,jj),kk);
    
    //Resize so that the full 3D array 
    //resize_to_3D_y(avg,*tmp2D);

    for (int j = avg.lbound(secondDim); j < avg.ubound(secondDim); j++){
        avg(blitz::Range::all(),j,blitz::Range::all()) = 
            (*tmp2D)(blitz::Range::all(),0,blitz::Range::all());
        }

    delete tmp2D;
    
}

void compute_average_x(TArrayn::DTArray & avg, const TArrayn::DTArray & field)
{
     int numprocs;
    //MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk; 

    //Local values of the grid size
    //int Nx_ = field.ubound(firstDim);
    int Ny_ = field.extent(secondDim);
    int Nz_ = field.extent(thirdDim);

    //Define the ranges for clarity
    blitz::Range yrange(field.lbound(secondDim),field.ubound(secondDim));
    blitz::Range zrange(field.lbound(thirdDim),field.ubound(thirdDim));

    //I think we can do this because of the storage ordering of DTArrays
    //It works. Someone else can change this to be consistent with compute_average_y()
    //if they want.

    Array<double,2> yzslice(Ny_,Nz_);
   
    // Mean in the x-direction owned by this process. 
    yzslice = mean(field(kk,ii,jj),kk);

    /* Note: Blitz stores the entries of the array in contiguous memory. This is good for us
       because we need to define neither a new MPI_Datatype nor a new MPI_Op. The approach to get the global average is
       to provide MPI with the address of the first element of the slice (which represents the 
       average owned by this process) and call MPI_Reduce to the root process. Thus, each process
       owns a 2D array containing the average across the x extent on that process EXCEPT the root process,
       which contains the average over the global x extent.
    */

    /**************************************************************************************************/
    /*  An opportunity for optimization in the future. If all we want to do is to compute the average
        and not any fluctuating quantity, we can do a MPI_Reduce, to the master process. That means 
        that the other processes are holding junk. This is not a problem if we only want to compute 
        the mean and nothing else, since write_average_x() only writes from the master process.
        However, if we are using compute_average_x as a step in another computation (like returning
        a fluctuation or computing the variance, say), then each process must hold the global x mean.
    */

    /* For the MPI_reduce, call the in-place option for the root process so we don't 
       need to define a new temporary array (note that MPI_IN_PLACE can only be called on the root process).
       On the other processes, we can use the same send and recv buffers since the other processes aren't reciving anything
    */

    /*
    if (master())
    {
        MPI_Reduce(MPI_IN_PLACE,&yzslice(0,0),Ny_*Nz_,
            MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
    }
    else
    {
        MPI_Reduce(&yzslice(0,0),&yzslice(0,0),Ny_*Nz_,
            MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD); 
    }

    
    if (master())
    {
        yzslice = yzslice/numprocs; //Compute the global mean on the master process
    }
    */

    /**************************************************************************************************/

    

    //The alternative to the above (and that which is more general) is to do the following:
    Array<double,2> recslice(Ny_,Nz_);
    MPI_Allreduce(&yzslice(0,0),&recslice(0,0),Ny_*Nz_,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD); 

    recslice = recslice/numprocs; //Compute the global mean
    
        
    //Resize to a full 3D array
    for (int i = avg.lbound(firstDim); i < avg.ubound(firstDim); i++){
    avg(i,blitz::Range::all(),blitz::Range::all()) = 
        recslice(blitz::Range::all(),blitz::Range::all());
    }

}


void write_average_x(const int plotnum, const std::string basename, const TArrayn::DTArray & field)
{
    /*
    Now we need to write the array appropriately.
    Unfortunately, we can't just call write array, since the writing is organized process by process.
    However, if we call write_array on only master(), then only the slice held by the master process will be 
    written. Thus, a 1xNyxNz array will be written
    */

        //Global values of the grid size
    if (master())
    {
        int Ny_ = field.extent(secondDim);
        int Nz_ = field.extent(thirdDim);

        DTArray * tmp2D;

        tmp2D = alloc_array(1,Ny_,Nz_,MPI_COMM_SELF);

        //Define the ranges for clarity
        blitz::Range yrange(field.lbound(secondDim),field.ubound(secondDim));
        blitz::Range zrange(field.lbound(thirdDim),field.ubound(thirdDim));
        (*tmp2D)(blitz::Range(0,0),yrange,zrange) = field(blitz::Range(0,0),yrange,zrange);

        TArrayn::write_array(*tmp2D,basename,plotnum,MPI_COMM_SELF);

        delete tmp2D;
    }

}

void write_average_y(const int plotnum, const std::string basename, const TArrayn::DTArray & field)
{
    /*
    When writing the y average, don't call master. 
    In general, wince writing the y_average is the same as writing other GNx/1/Nz
    arrays, we don't strictly need this, but I think it's important to have consistency in the
    ways that x and y averages are called. 
    */
        int GNx = 0; //Global Nx
        int Nx_ = field.extent(firstDim); //Local Nx
        int Nz_ = field.extent(thirdDim); //Local Nz (same as global Nz)
        DTArray *tmp2D;

        MPI_Allreduce(&Nx_,&GNx,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

        tmp2D = alloc_array(GNx,1,Nz_);

        //Define the ranges for clarity
        blitz::Range xrange(field.lbound(firstDim),field.ubound(firstDim));
        blitz::Range zrange(field.lbound(thirdDim),field.ubound(thirdDim));

        //Assign a slice of the averaged field to the "2D" DTArray. Assumes every x-z slice of field is the same
        (*tmp2D)(xrange,blitz::Range(0,0),zrange) = field(xrange,blitz::Range(0,0),zrange);

        TArrayn::write_array(*tmp2D,basename,plotnum);

        delete tmp2D;
}

void write_average_profile(const int plotnum, const std::string basename, const TArrayn::DTArray & field)
{
    /*
    To write a profile of a DTArray, we just need to specify the appropriate array dimensions
    and call it just like write_average_x
    This is intended to be used after various averages are taken. If both an x and y average are
    taken, then every profile will be identical. So take the first entry.
    */
    
        //Global values of the grid size
    if (master())
    {
        
        int Nz_ = field.extent(thirdDim);

        DTArray * tmp1D;

        tmp1D = alloc_array(1,1,Nz_,MPI_COMM_SELF);

        //Define the ranges for clarity
        blitz::Range zrange(field.lbound(thirdDim),field.ubound(thirdDim));
        (*tmp1D)(blitz::Range(0,0),blitz::Range(0,0),zrange) = 
            field(blitz::Range(0,0),blitz::Range(0,0),zrange);


        TArrayn::write_array(*tmp1D,basename,plotnum,MPI_COMM_SELF);

        delete tmp1D;
    }

}

void compute_variance(TArrayn::DTArray & variance, const TArrayn::DTArray & field,
    void (*direction)(TArrayn::DTArray &, const TArrayn::DTArray &))
{

        //Note that compute variance is special in that it takes a pointer to the appropriate
        //averaging function. Since computing the variance in either direction is the same, no sense
        //in writing two nearly identical functions.    
         
         
        //Var(u) = < (u - <u>)^2 >
        //       = < u^2 - 2<u>u + <u>^2 >
        //       = <u^2> -2<u>^2 + <u>^2
        //       = <u^2> - <u>^2      <---- We do this in this function


        int GNx = 0; //Global Nx
        int Nx_ = variance.extent(firstDim); //Local Nx
        int Ny_ = variance.extent(secondDim); //Local Ny (same as global Ny)
        int Nz_ = variance.extent(thirdDim); //Local Nz (same as global Nz)
        
        DTArray * temp1, * temp2;
        MPI_Allreduce(&Nx_,&GNx,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

        temp1 = alloc_array(GNx,Ny_,Nz_);
        temp2 = alloc_array(GNx,Ny_,Nz_);

        //compute the mean in the spanwise
        direction(*temp1,field); //copmpute average of field
        *temp1 = pow2(*temp1); //Compute (average of field) squared

        *temp2 = pow2(field); //Save squared field
        direction(variance,*temp2); //Compute average of (squared field)

        variance = variance - (*temp1); //compute variance by taking the difference
        
        //important....
        delete temp1, temp2;

        //To write the variance, call the appropriate write_average function
}
